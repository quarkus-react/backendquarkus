package org.sales.staff.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import org.sales.staff.model.entity.BillEntity;

@ApplicationScoped
public class BillRepository implements PanacheRepository<BillEntity> {

}
