package org.sales.staff.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import org.sales.staff.model.entity.PersonEntity;

@ApplicationScoped
public class PersonRepository implements PanacheRepository<PersonEntity> {
    public PersonEntity findByIdentificacion(String identificacion) {
        return find("identificacion", identificacion).firstResult();
    }
}
