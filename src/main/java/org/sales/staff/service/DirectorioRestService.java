package org.sales.staff.service;

import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.sales.staff.model.entity.BillEntity;
import org.sales.staff.model.entity.PersonEntity;
import org.sales.staff.model.request.PersonRequest;
import org.sales.staff.repository.BillRepository;
import org.sales.staff.repository.PersonRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class DirectorioRestService {

    @Inject
    private PersonRepository personRepository;
    private BillRepository billRepository;

    public List<PersonEntity> findPersonas(){
         return personRepository.listAll();
    }

    public PersonEntity findPersonaByIdentification(String id){
        return personRepository.findByIdentificacion(id);
    }

    @Transactional
    public ResponseEntity<?> storePerson(PersonRequest personRequest){
        try {
            PersonEntity personEntity = new PersonEntity();
            personEntity.setNombre(personRequest.getNombre());
            personEntity.setApp_paterno(personRequest.getApp_paterno());
            personEntity.setApp_materno(personRequest.getApp_materno());
            personEntity.setIdentificacion(personRequest.getIdentificador());

            personRepository.persist(personEntity);
            return ResponseEntity.status(HttpStatus.CREATED).body("Persona insertada correctamente");
        } catch (Exception e){
            log.error("An error occurred while saving person: "+ e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Transactional
    public Boolean deletePersonaByIdentification(String identifiation){
        PersonEntity person = personRepository.findByIdentificacion(identifiation);
        if (person!= null){
            personRepository.delete(person);
            return true;
        }
        log.info("Esa persona no se encuentra registrada");
        return false;

    }


}
