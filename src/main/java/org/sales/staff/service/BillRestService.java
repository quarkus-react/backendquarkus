package org.sales.staff.service;

import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.sales.staff.model.entity.BillEntity;
import org.sales.staff.model.entity.PersonEntity;
import org.sales.staff.model.request.BillRequest;
import org.sales.staff.repository.BillRepository;
import org.sales.staff.repository.PersonRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class BillRestService {

    @Inject
    BillRepository billRepository;
    @Inject
    PersonRepository personRepository;

    public List<BillEntity> findFacturasByPersona(String identification){
        PersonEntity person = personRepository.findByIdentificacion(identification);
        if (person!=null){
            return person.getFacturas();
        }
        return Collections.EMPTY_LIST;
    }

    @Transactional
    public ResponseEntity<?> storeBill(BillRequest billRequest){
        try {
            BillEntity billEntity = new BillEntity();
            billEntity.setMonto(billRequest.getMonto());

            Optional<PersonEntity> optionalPerson = Optional.ofNullable(personRepository.findById((long) billRequest.getPersonaId()));
            if (optionalPerson.isPresent()){
                billEntity.setPersona(optionalPerson.get());
                billEntity.setFecha(Date.from(Instant.now()));

                billRepository.persist(billEntity);
                return ResponseEntity.status(HttpStatus.CREATED).body("Factura creada exitosamente");
            } else {
                log.error("No se encontró la persona con ID " + billRequest.getPersonaId());
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No se encontró la persona con ID " + billRequest.getPersonaId());
            }
        } catch (Exception e){
            log.error("An error occurred while saving bill record " + e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }



}






