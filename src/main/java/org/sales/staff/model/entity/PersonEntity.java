package org.sales.staff.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "persona")
public class PersonEntity extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String nombre;
    @Column(name = "apellido_paterno")
    private String app_paterno;
    @Column(name = "apellido_materno")
    private String app_materno;
    @Column(name = "identificacion")
    private String identificacion;

    @JsonIgnore
    @OneToMany(mappedBy = "persona",cascade = CascadeType.ALL,orphanRemoval = true)
    private List<BillEntity> facturas;
}
