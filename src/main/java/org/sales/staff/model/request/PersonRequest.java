package org.sales.staff.model.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonRequest {
    private int id;
    private String nombre;
    private String app_paterno;
    private String app_materno;
    private String identificador;
}
