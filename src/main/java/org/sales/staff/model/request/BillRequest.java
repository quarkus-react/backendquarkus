package org.sales.staff.model.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BillRequest {
    private double monto;
    private int personaId;
}
