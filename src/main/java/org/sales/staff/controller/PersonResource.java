package org.sales.staff.controller;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import org.sales.staff.model.entity.PersonEntity;
import org.sales.staff.model.request.PersonRequest;
import org.sales.staff.service.DirectorioRestService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/person")
public class PersonResource {

    @Inject
    DirectorioRestService directorioRestService;

    @GET
    public List<?> findPersonas(){
        return directorioRestService.findPersonas();
    }

    @GetMapping("/search/{id}")
    public PersonEntity findPersonaById(@PathVariable String id){
        return directorioRestService.findPersonaByIdentification(id);
    }

    @PostMapping("/insert")
    public ResponseEntity post(PersonRequest personRequest){
        try {
            ResponseEntity<?> responseEntity = directorioRestService.storePerson(personRequest);
            if (responseEntity.getStatusCode().is2xxSuccessful()){
                log.info("New person inserted successfully");
                return new ResponseEntity<>(responseEntity,HttpStatus.OK);
            }else {
                log.info("Person not inserted");
                return new ResponseEntity<>("An error occurred while creating person",HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } catch (Exception e){
            log.error("An error occurred while creating person " + e);
            return new ResponseEntity<>("An error occurred while creatig person", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable String id){
        try {
            if (directorioRestService.deletePersonaByIdentification(id)){
                log.error("Person record deleted successfully");
                return new ResponseEntity<>("Person deleted successfully",HttpStatus.OK);
            } else {
                log.error("Persona no encontrada/ no registrada");
                return new ResponseEntity<>("Error while deleting person record",HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            log.error("An error occurred while deleting person with identification {}",id);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



}
