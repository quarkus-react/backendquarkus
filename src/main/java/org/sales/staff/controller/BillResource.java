package org.sales.staff.controller;

import jakarta.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.sales.staff.model.entity.BillEntity;
import org.sales.staff.model.entity.PersonEntity;
import org.sales.staff.model.request.BillRequest;
import org.sales.staff.repository.PersonRepository;
import org.sales.staff.service.BillRestService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/bill")
public class BillResource {

    @Inject
    BillRestService billRestService;
    PersonRepository personRepository;


    @GetMapping("/search/{identification}")
    public ResponseEntity<?> findBillsByPerson(@PathVariable String identification){
        List<BillEntity> bills = billRestService.findFacturasByPersona(identification);
        if (bills.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No se encontraron facturas para la persona con id: "+ identification);
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(bills);
        }
    }


    @PostMapping("/create")
    public ResponseEntity post(BillRequest billRequest){
        try{
            ResponseEntity<?> responseEntity = billRestService.storeBill(billRequest);
            if (responseEntity.getStatusCode().is2xxSuccessful()){
                log.info("New bill created successfully");
                return new ResponseEntity<>(responseEntity,HttpStatus.OK);
            } else {
                log.info("Bill not created");
                return new ResponseEntity<>("An error occurred while creating bill",HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } catch (Exception e){
            log.error("An error occurred while creating bill");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
